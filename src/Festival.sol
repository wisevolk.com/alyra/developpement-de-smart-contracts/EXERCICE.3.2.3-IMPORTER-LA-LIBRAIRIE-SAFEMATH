pragma solidity >=0.4.22 <0.7.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Cogere {

    using SafeMath for uint;

    mapping(address => uint) organisateurs;

    constructor() internal {
        organisateurs[msg.sender] = 100;
    }

    function transferOrga(address orga, uint parts) public {
        require(organisateurs[msg.sender].sub(parts) < organisateurs[msg.sender], "Vous ne possèdez pas les parts nécessaires!");
        organisateurs[msg.sender] = organisateurs[msg.sender].sub(parts);
        organisateurs[orga] = parts;
    }

    function estOrga(address orga) public view returns (bool) {
        return organisateurs[orga] > 0;
    }
}

contract CagnotteFestival is Cogere{

    mapping(address => bool) festivaliers;
    mapping(string => uint) sponsors;
    uint private placeRestantes;
    uint private depensesTotales;

    constructor(uint _places) public {
        placeRestantes = _places;
    }

    function acheterTicket() public payable {
        require(msg.value >= 500 finney, "Place à 0.5 ethers");
        require(placeRestantes > 0,"Plus de places !");
        festivaliers[msg.sender] = true;
        placeRestantes.sub(1);
    }

    function payer(address payable destinataire, uint montant) public {
        require(estOrga(msg.sender));
        require(destinataire != address(0));
        require(montant > 0);
        destinataire.transfer(montant);
    }

    function sponsoriser(string memory _nom) public payable {
        require(msg.value >= 30 ether,"La mise d'entrée est de 30 ether minimum");
        sponsors[_nom] = msg.value;
    }
}
